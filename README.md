# Common notes:
## design patterns
- Dzone refcards "Design patterns"
- [sourcemaking]
- [refactoring.guru]
- [patterns.instantinterfaces.nl]
- książka Effective Java
- książka Java Concurrency In practice
# Pytania
## 1. Jak poradzić sobie z wieloma parametrami konstruktora?
- czasami kilka parametrów można wciągnąć do jednego typu
- Builder pattern
### 1.1 jak można osiągnąć "nazwane konstruktory"?
Static factory methods
#### Zalety:
- mają nazwę
- nie zawsze trzeba tworzyć nowy obiekt
- mogą zwracać podtyp
- przed operatorem <> pomagały unikać podwójnego wskazania typu generycznego
#### Wady:
- jeśli nie ma konstruktora protected to nie można klasy rozszerzyć
- nie są łatwo odróżnialne od innych statycznych metod
## 2. Jak w Javie stworzyć singeton
### 2.1 co robi volatile?
## 3. jakie znasz metody refaktoryzacji
## 4. Jak zrefaktoryzować ten kod:

```java
public class MyClass {
    @Autowired
    private ConvertAToB convertAToB;
    @Autowired
    private ConvertBToC convertBToC;
    @Autowired
    private ConvertCToD convertCToD;
    public Optional<ObjectD> convertAToD(ObjectA objectA) {
        if (objectA != null) {
            Optional<ObjectB> objectB = convertAToB.convertToB(objectA);
            if (objectB.isPresent()) {
                ObjectC objectC = convertBToC.convertToC(objectB.get());
                if (objectC != null) {
                    ObjectD objectD = convertCToD.convertToD(objectC);
                    return Optional.ofNullable(objectD);
                }
            }
        }
        return Optional.empty();
    }
}
```

### Skorzystać z Optional?
   - A co z zasadami mówiącymi aby tego nie robić ([prezentacja Stuarta Marksa](https://virtualjug.com/vjug24-session-optional-the-mother-of-all-bikesheds-by-stuart-marks/))
   - Więcej o Optional także pod [expert-java-8-Optional-presentation](http://slides.codefx.org/expert-java-8/index.html)
:
```Java
public class MyClass {
    @Autowired
    private ConvertAToB convertAToB;
    @Autowired
    private ConvertBToC convertBToC;
    @Autowired
    private ConvertCToD convertCToD;
    public Optional<ObjectD> convertAToD(ObjectA objectA) {
        return Optional.ofNullable(objectA)
            .flatMap(convertAToB::convertToB)
            .map(convertBToC::convertToC)
            .map(convertCToD::convertToD);
    }
}
```

## 5. Rozszerzanie implementacji:

```Java
public final class Trade {
    private final Date timestamp;
    private final Symbol symbol;
    private final BigDecimal price;
    public Trade(Date timestamp, Symbol symbol, BigDecimal price) {
        this.timestamp = timestamp;
        this.symbol = symbol;
        this.price = price;
    }
    public boolean isBelow(Trade trade) {
        return price.compareTo(trade.price) < 0;
    }
    public boolean isOffMarket() {
            return !symbol.isTradingAt(timestamp);
        }
}
```

Jak w usłudze obsługującej handel (public void handleTrade(Trade aTrade); możemy śledzić zmiany cen dla każdego symbolu. Nie wolno jednak opublikować pól klasy Trade ponieważ są one szczegółem implementacyjnym i mogą się zmienić.

### Loan pattern:
1. Rozszerzyć Trade o Mapę (cache) Trade'ów
2. Wprowadzić w Trade metodę, która ma zaakceptuje konsumenta nowego Trade i starego Trade dla tego samego symbolu.
```Java
        public void withNewTrade(Trade newTrade, BiConsumer < Trade, Trade > logic) {
            Trade latestTrade = latestTrades.get(newTrade.symbol);
            if (latestTrade != null) {
                logic.accept(latestTrade, newTrade);
            }
            latestTrades.put(newTrade.symbol, newTrade);
        }
```

Teraz handle trade wygląda tak:
```Java
public void handleTrade(Trade trade) {
    tradeCache.withNewTrade(trade, (lastTrade, newTrade) - > ...logic...);
}
```

## 6. Optional vs Monad
https://www.sitepoint.com/how-optional-breaks-the-monad-laws-and-why-it-matters/

## 7. Handle exceptions bez throws
Jak do przykładu poniżej wprowadzić obsługę wyjątków w funkcji jak poniżej?

```Java
    private void withSessionFactory( Consumer< SessionFactory > aConsumerFunction )
    {
        SessionFactory sf = new SessionFactory();
        try
        {
            aConsumerFunction.accept( sf );
        }
        finally
        {
            sf.close();
        }
    }
```

Można zaimplementować jako:

* private void withSessionFactory( Consumer< SessionFactory > aConsumerFunction , Consumer<Exception> aExceptionHandler)
* private Try<?> withSessionFactory( Consumer< SessionFactory > aConsumerFunction)


[sourcemaking]: http://sourcemaking.com
[refactoring.guru]: (http://refactoring.guru)
[patterns.instantinterfaces.nl]: (http://patterns.instantinterfaces.nl/current/Refactoring-and-Design-Patterns-PAT.html#PAT)
